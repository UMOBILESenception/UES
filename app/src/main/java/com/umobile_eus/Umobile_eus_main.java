package com.umobile_eus;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Umobile_eus_main extends Activity {

    Button oi, now, pml, route, context, direct, sharing, migration;
    boolean is_oi_active,is_now_active, is_pml_active, is_route_active, is_context_active, is_direct_active, is_sharing_active, is_migration_active = false;
    int backButtonCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_umobile_eus_main);

        oi = (Button) findViewById(R.id.bt_oi);
        now = (Button) findViewById(R.id.bt_now);
        pml = (Button) findViewById(R.id.bt_pml);
        route = (Button) findViewById(R.id.bt_route);
        context = (Button) findViewById(R.id.bt_contextualization);
        direct = (Button) findViewById(R.id.bt_direct);
        sharing = (Button) findViewById(R.id.bt_sharing);
        migration = (Button) findViewById(R.id.bt_service);

        oi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!is_oi_active) {

                    String package_name = "com.copelabs.android.oi";
                    start_external_app(package_name);

                    oi.setBackgroundResource(R.drawable.button_pressed_corner);
                    is_oi_active = true;
                }
                else{
                    oi.setBackgroundResource(R.drawable.button_corner);
                    is_oi_active = false;
                }

            }
        });

        now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!is_now_active) {

                    String package_name = "pt.ulusofona.copelabs.now";
                    start_external_app(package_name);

                    context.setBackgroundResource(R.drawable.button_pressed_corner);
                    direct.setBackgroundResource(R.drawable.button_pressed_corner);
                    now.setBackgroundResource(R.drawable.button_pressed_corner);
                    is_now_active = true;
                }
                else{
                    context.setBackgroundResource(R.drawable.button_corner);
                    direct.setBackgroundResource(R.drawable.button_corner);
                    now.setBackgroundResource(R.drawable.button_corner);
                    is_now_active = false;
                }
            }
        });

        pml.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!is_pml_active) {

                    String package_name = "com.senception.persenselight";
                    start_external_app(package_name);

                    pml.setBackgroundResource(R.drawable.button_pressed_corner);
                    is_pml_active = true;
                }
                else{
                    pml.setBackgroundResource(R.drawable.button_corner);
                    is_pml_active = false;
                }
            }
        });

        route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!is_route_active) {
                    route.setBackgroundResource(R.drawable.button_pressed_corner);
                    is_route_active = true;
                }
                else{
                    route.setBackgroundResource(R.drawable.button_corner);
                    is_route_active = false;
                }
            }
        });

        context.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!is_context_active) {
                    context.setBackgroundResource(R.drawable.button_pressed_corner);
                    is_context_active = true;
                }
                else{
                    context.setBackgroundResource(R.drawable.button_corner);
                    is_context_active = false;
                }
            }
        });

        direct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!is_direct_active) {
                    direct.setBackgroundResource(R.drawable.button_pressed_corner);
                    is_direct_active = true;
                }
                else{
                    direct.setBackgroundResource(R.drawable.button_corner);
                    is_direct_active = false;
                }
            }
        });

        sharing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!is_sharing_active) {
                    sharing.setBackgroundResource(R.drawable.button_pressed_corner);
                    is_sharing_active = true;
                }
                else{
                    sharing.setBackgroundResource(R.drawable.button_corner);
                    is_sharing_active = false;
                }
            }
        });

        migration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!is_migration_active) {
                    migration.setBackgroundResource(R.drawable.button_pressed_corner);
                    is_migration_active = true;
                }
                else{
                    migration.setBackgroundResource(R.drawable.button_corner);
                    is_migration_active = false;
                }
            }
        });
    }

    public void start_external_app(String package_name){

        PackageManager package_manager = getPackageManager();

        try{
            Intent intent = package_manager.getLaunchIntentForPackage(package_name);
            if(intent != null){
                startActivity(intent);
            }
        }
        catch (ActivityNotFoundException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed(){
        if(backButtonCount >= 1)
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this, getString(R.string.mainback), Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }

}
